const simplelog = function (text) {
    console.log(`${Date()}: ${text}`)
}

module.exports = simplelog