const removeDupes = require('./removeDupes.js')

let testData = [
    {
    "_id": "jkj238238jdsnfsj23",
    "email": "foo@bar.com",
    "firstName":  "John",
    "lastName": "Smith",
    "address": "123 Street St",
    "entryDate": "2014-05-07T17:30:20+00:00"
    },
    {
    "_id": "wabaj238238jdsnfsj23",
    "email": "foo@bar.com",
    "firstName":  "Fran",
    "lastName": "Jones",
    "address": "8803 Dark St",
    "entryDate": "2014-05-07T17:31:20+00:00"
    }
]

let array = removeDupes(testData, 'email')
if (array.length === 1) {
    console.log('Passed!')
} else {
    console.log('Failed!')
}
if (array[0]._id === 'wabaj238238jdsnfsj23') {
    console.log('Passed!')
} else {
    console.log('Failed!')
}

testData = [
    {
    "_id": "jkj238238jdsnfsj23",
    "email": "foo@bar.com",
    "firstName":  "John",
    "lastName": "Smith",
    "address": "123 Street St",
    "entryDate": "2014-05-07T17:31:20+00:00"
    },
    {
    "_id": "jkj238238jdsnfsj23",
    "email": "cool@bar.com",
    "firstName":  "Fran",
    "lastName": "Jones",
    "address": "8803 Dark St",
    "entryDate": "2014-05-07T17:30:20+00:00"
    }
]

array = removeDupes(testData, '_id')
if (array.length === 1) {
    console.log('Passed!')
} else {
    console.log('Failed!')
}

if (array[0].email === 'foo@bar.com') {
    console.log('Passed!')
} else {
    console.log('Failed!')
}


