const simpleLog = require('./simpleLog.js')

const removeDuplicates = function (array, compare) {
    const trackUniques = {}
    const unique = []
    array.forEach((data, currentIndex) => {
        if(!trackUniques[data[compare]]) {
            simpleLog(`Found unique ${compare}: ${data[compare]}`)
            trackUniques[data[compare]] = {
                data,
                currentIndex
            };
            simpleLog(`Adding data to the list of unique: ${data[compare]}`)
            unique.push(data)
        } else {
            simpleLog(`Record already exists: ${[data[compare]]}`)
            const savedData = trackUniques[data[compare]].data
            savedUnixDate = new Date(savedData.entryDate).getTime() / 1000
            currentDataUnixDate = new Date(data.entryDate).getTime() / 1000

            if (savedUnixDate === currentDataUnixDate) {
                simpleLog(`There is a match between dates, keeping the record that appears last in the list`)
                unique[trackUniques[data[compare]].currentIndex] = data
                trackUniques[data[compare]].data = data
            } else if (currentDataUnixDate > savedUnixDate) {
                simpleLog(`Switching the record with the most recent date`)
                unique[trackUniques[data[compare]].currentIndex] = data
                trackUniques[data[compare]].data = data
            }
        }
    })

    return unique
}

module.exports = removeDuplicates
