const removeDuplicates = require('./lib/removeDupes.js')
const simpleLog = require('./lib/simpleLog.js')
const config = require('./config.json')
const leadsJson = require(config.filename)
const dupFieldsToRemove = config.dupesFieldsToRemove

const removeDupes = (array, dupeIds, length) => {
    if (!length) {
        return array
    } 
    simpleLog(`Getting Unique array by ${dupeIds[length - 1]}`)
    const uniques = removeDuplicates(array, dupeIds[length - 1])
    return removeDupes(uniques, dupeIds, length - 1)
}

console.log(removeDupes(leadsJson.leads, dupFieldsToRemove, 2))